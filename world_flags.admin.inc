<?php

/**
 * @file
 * Admin wizard to configure World Flags Module.
 */

/**
 * Returns the list of steps and their associated forms.
 */
function _form_world_flags_steps() {
  return array(
      1 => array(
        'form' => 'form_world_flags_basic_configuration',
      ),
    );
}

function form_world_flags_wizard($form, &$form_state) {

  // Initialize a description of the steps for the wizard.
  if (empty($form_state['step'])) {
    $form_state['step'] = 1;

    // This array contains the function to be called at each step to get the
    // relevant form elements. It will also store state information for each
    // step.
    $form_state['step_information'] = _form_world_flags_steps();
  }
  $step = &$form_state['step'];
  drupal_set_title(t('World Flags Wizard: Step @step/@stepcount', array('@step' => $step, '@stepcount' => count($form_state['step_information']))));

  // Call the function named in $form_state['step_information'] to get the
  // form elements to display for this step.
  $form = $form_state['step_information'][$step]['form']($form, $form_state);

  // Show the 'previous' button if appropriate. Note that #submit is set to
  // a special submit handler, and that we use #limit_validation_errors to
  // skip all complaints about validation when using the back button. The
  // values entered will be discarded, but they will not be validated, which
  // would be annoying in a "back" button.
  if ($step > 1) {
    $form['prev'] = array(
      '#type' => 'submit',
      '#value' => t('Previous'),
      '#name' => 'prev',
      '#submit' => array('form_world_flags_wizard_previous_submit'),
      '#limit_validation_errors' => array(),
    );
  }

  // Show the Next button only if there are more steps defined.
  if ($step < count($form_state['step_information'])) {
    // The Next button should be included on every step
    $form['next'] = array(
      '#type' => 'submit',
      '#value' => t('Next'),
      '#name' => 'next',
      '#submit' => array('form_world_flags_wizard_next_submit'),
    );
  }
  else {
    // Just in case there are no more steps, we use the default submit handler
    // of the form wizard. Call this button Finish, Submit, or whatever you
    // want to show. When this button is clicked, the
    // form_example_wizard_submit handler will be called.
    $form['finish'] = array(
      '#type' => 'submit',
      '#value' => t('Finish'),
    );
  }

  // Include each validation function defined for the different steps.
  if (function_exists($form_state['step_information'][$step]['form'] . '_validate')) {
    $form['next']['#validate'] = array($form_state['step_information'][$step]['form'] . '_validate');
  }

  return $form;
}

function form_world_flags_wizard_previous_submit($form, &$form_state) {
  $current_step = &$form_state['step'];
  $form_state['step_information'][$current_step]['stored_values'] = $form_state['values'];
  if ($current_step > 1) {
    $current_step--;
    $form_state['values'] = $form_state['step_information'][$current_step]['stored_values'];
  }
  $form_state['rebuild'] = TRUE;
}

function form_world_flags_wizard_next_submit($form, &$form_state) {
  $current_step = &$form_state['step'];
  $form_state['step_information'][$current_step]['stored_values'] = $form_state['values'];

  if ($current_step < count($form_state['step_information'])) {
    $current_step++;
    if (!empty($form_state['step_information'][$current_step]['stored_values'])) {
      $form_state['values'] = $form_state['step_information'][$current_step]['stored_values'];
    }
    else {
      $form_state['values'] = array();
    }
    $form_state['rebuild'] = TRUE;  // Force rebuild with next step.
    return;
  }
}

function form_world_flags_basic_configuration($form, &$form_state) {
  $form = array();

  // Fieldset for block options.
  $form['basic'] = array(
    '#type' => 'fieldset',
    '#title' => t('Display options'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  // Switch to display World Flags graphic in a block.
  $form['basic']['world_flags_block'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use World Flags graphics in a block?'),
    '#default_value' => isset($form_state['step_information']['1']['stored_values']['world_flags_block']) ?
      $form_state['step_information']['1']['stored_values']['world_flags_block'] :
      variable_get('world_flags_block', TRUE),
  );

  // Switch to display World Flags graphic as favicon.
  $form['basic']['world_flags_favicon'] = array(
    '#type' => 'checkbox',
    '#title' => t('Replace/override active favicon with World Flags favicon?'),
    '#default_value' => isset($form_state['step_information']['1']['stored_values']['world_flags_favicon']) ?
      $form_state['step_information']['1']['stored_values']['world_flags_favicon'] :
      variable_get('world_flags_favicon', TRUE),
  );

  // Switch to display World Flags graphic as the logo.
  $form['basic']['world_flags_logo'] = array(
    '#type' => 'checkbox',
    '#title' => t('Replace/override active logo with World Flags logo?'),
    '#default_value' => isset($form_state['step_information']['1']['stored_values']['world_flags_logo']) ?
      $form_state['step_information']['1']['stored_values']['world_flags_logo'] :
      variable_get('world_flags_logo', TRUE),
  );

  // Fieldset for block options.
  $form['display_options_block'] = array(
    '#type' => 'fieldset',
    '#title' => t('Block options'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#states' => array(
      'invisible' => array(
        ":input[name='world_flags_block']" => array('checked' => FALSE),
      ),
    ),
  );

  $form['display_options_block']['world_flags_block_type'] = array(
    '#title' => t("The type of flag to be displayed in a block"),
    '#type' => 'select',
    '#options' => array(
      'animated' => t('Animated'),
      'non-animated' => t('Non-animated'),
      'badge' => t('Badge'),
    ),
    '#description' => t('Choose animated graphics, non-animated graphics, or a non-animated badge.'),
    '#default_value' => isset($form_state['step_information']['1']['stored_values']['world_flags_block_type']) ?
      $form_state['step_information']['1']['stored_values']['world_flags_block_type'] :
      variable_get('world_flags_block_type', 'badge'),
  );

  $form['display_options_block']['world_flags_block_distance'] = array(
    '#title' => t("Distance between viewer and flag"),
    '#type' => 'select',
    '#options' => array(
      'close' => t('Close'),
      'far' => t('Far'),
    ),
    '#description' => t('Choose if you want to view the flags from a far distance or close up.'),
    '#default_value' => isset($form_state['step_information']['1']['stored_values']['world_flags_block_distance']) ?
      $form_state['step_information']['1']['stored_values']['world_flags_block_distance'] :
      variable_get('world_flags_block_distance', 'far'),
    '#states' => array(
      'invisible' => array(
        ":input[name='world_flags_block_type']" => array('value' => 'badge'),
      ),
    ),
  );

  $form['display_options_block']['world_flags_block_background_color'] = array(
    '#title' => t("Graphics background color"),
    '#type' => 'select',
    '#options' => array(
      'blue' => t('Blue'),
      'black' => t('Black'),
    ),
    '#description' => t('Choose the background color of the graphic.'),
    '#default_value' => isset($form_state['step_information']['1']['stored_values']['world_flags_block_background_color']) ?
      $form_state['step_information']['1']['stored_values']['world_flags_block_background_color'] :
      variable_get('world_flags_block_background_color', 'black'),
    '#states' => array(
      'invisible' => array(
        ":input[name='world_flags_block_type']" => array('value' => 'badge'),
      ),
    ),
  );

  // Fieldset for favicon options.
  $form['display_options_favicon'] = array(
    '#type' => 'fieldset',
    '#title' => t('Favicon options'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#states' => array(
      'invisible' => array(
        ":input[name='world_flags_favicon']" => array('checked' => FALSE),
      ),
    ),
  );

  $form['display_options_favicon']['world_flags_favicon_type'] = array(
    '#title' => t("The type of flag to be displayed for your favicon"),
    '#type' => 'select',
    '#options' => array(
      'animated' => t('Animated'),
      'non-animated' => t('Non-animated'),
      'badge' => t('Badge'),
    ),
    '#description' => t('Choose animated graphics, non-animated graphics, or a non-animated badge.'),
    '#default_value' => isset($form_state['step_information']['1']['stored_values']['world_flags_favicon_type']) ?
      $form_state['step_information']['1']['stored_values']['world_flags_favicon_type'] :
      variable_get('world_flags_favicon_type', 'animated'),
  );

  $form['display_options_favicon']['world_flags_favicon_distance'] = array(
    '#title' => t("Distance between viewer and flag"),
    '#type' => 'select',
    '#options' => array(
      'close' => t('Close'),
      'far' => t('Far'),
    ),
    '#description' => t('Choose if you want to view the flags from a far distance or close up.'),
    '#default_value' => isset($form_state['step_information']['1']['stored_values']['world_flags_favicon_distance']) ?
      $form_state['step_information']['1']['stored_values']['world_flags_favicon_distance'] :
      variable_get('world_flags_favicon_distance', 'close'),
    '#states' => array(
      'invisible' => array(
        ":input[name='world_flags_favicon_type']" => array('value' => 'badge'),
      ),
    ),
  );

  $form['display_options_favicon']['world_flags_favicon_background_color'] = array(
    '#title' => t("Graphics background color"),
    '#type' => 'select',
    '#options' => array(
      'blue' => t('Blue'),
      'black' => t('Black'),
    ),
    '#description' => t('Choose the background color of the graphic.'),
    '#default_value' => isset($form_state['step_information']['1']['stored_values']['world_flags_favicon_background_color']) ?
      $form_state['step_information']['1']['stored_values']['world_flags_favicon_background_color'] :
      variable_get('world_flags_favicon_background_color', 'black'),
    '#states' => array(
      'invisible' => array(
        ":input[name='world_flags_favicon_type']" => array('value' => 'badge'),
      ),
    ),
  );

  $form['display_options_favicon']['overriding_favicon_options']['world_flags_favicon_visibility_override'] = array(
    '#type' => 'radios',
    '#title' => t('Show World Flags favicon on specific pages'),
    '#options' => array('except_listed' => t('All pages except those listed'), 'only_listed' => t('Only the listed pages')),
    '#default_value' => isset($form_state['step_information']['1']['stored_values']['world_flags_favicon_visibility_override']) ?
      $form_state['step_information']['1']['stored_values']['world_flags_favicon_visibility_override'] :
      variable_get('world_flags_favicon_visibility_override', 'except_listed'),
  );

  $form['display_options_favicon']['overriding_favicon_options']['world_flags_favicon_visibility_override_pages_list'] = array(
    '#type' => 'textarea',
    '#description' => t('Choose pages to display the World Flags favicon or pages not to display.'),
    '#default_value' => isset($form_state['step_information']['1']['stored_values']['world_flags_favicon_visibility_override_pages_list']) ?
      $form_state['step_information']['1']['stored_values']['world_flags_favicon_visibility_override_pages_list'] :
      variable_get('world_flags_favicon_visibility_override_pages_list', ''),
  );

  // Fieldset for logo options.
  $form['display_options_logo'] = array(
    '#type' => 'fieldset',
    '#title' => t('Logo options'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#states' => array(
      'invisible' => array(
        ":input[name='world_flags_logo']" => array('checked' => FALSE),
      ),
    ),
  );

  $form['display_options_logo']['world_flags_logo_type'] = array(
    '#title' => t("The type of flag to be displayed in the logo area"),
    '#type' => 'select',
    '#options' => array(
      'animated' => t('Animated'),
      'non-animated' => t('Non-animated'),
      'badge' => t('Badge'),
    ),
    '#description' => t('Choose animated graphics, non-animated graphics, or a non-animated badge.'),
    '#default_value' => isset($form_state['step_information']['1']['stored_values']['world_flags_logo_type']) ?
      $form_state['step_information']['1']['stored_values']['world_flags_logo_type'] :
      variable_get('world_flags_logo_type', 'animated'),
  );

  $form['display_options_logo']['world_flags_logo_distance'] = array(
    '#title' => t("Distance between viewer and flag"),
    '#type' => 'select',
    '#options' => array(
      'close' => t('Close'),
      'far' => t('Far'),
    ),
    '#description' => t('Choose if you want to view the flags from a far distance or close up.'),
    '#default_value' => isset($form_state['step_information']['1']['stored_values']['world_flags_logo_distance']) ?
      $form_state['step_information']['1']['stored_values']['world_flags_logo_distance'] :
      variable_get('world_flags_logo_distance', 'far'),
    '#states' => array(
      'invisible' => array(
        ":input[name='world_flags_logo_type']" => array('value' => 'badge'),
      ),
    ),
  );

  $form['display_options_logo']['world_flags_logo_background_color'] = array(
    '#title' => t("Graphics background color"),
    '#type' => 'select',
    '#options' => array(
      'blue' => t('Blue'),
      'black' => t('Black'),
    ),
    '#description' => t('Choose the background color of the graphic.'),
    '#default_value' => isset($form_state['step_information']['1']['stored_values']['world_flags_logo_background_color']) ?
      $form_state['step_information']['1']['stored_values']['world_flags_logo_background_color'] :
      variable_get('world_flags_logo_background_color', 'black'),
    '#states' => array(
      'invisible' => array(
        ":input[name='world_flags_logo_type']" => array('value' => 'badge'),
      ),
    ),
  );

  $form['display_options_logo']['overriding_logo_options']['world_flags_logo_visibility_override'] = array(
    '#type' => 'radios',
    '#title' => t('Show World Flags logo on specific pages'),
    '#options' => array('except_listed' => t('All pages except those listed'), 'only_listed' => t('Only the listed pages ')),
    '#default_value' => isset($form_state['step_information']['1']['stored_values']['world_flags_logo_visibility_override']) ?
      $form_state['step_information']['1']['stored_values']['world_flags_logo_visibility_override'] :
      variable_get('world_flags_logo_visibility_override', 'except_listed'),
  );

  $form['display_options_logo']['overriding_logo_options']['world_flags_logo_visibility_override_pages_list'] = array(
    '#type' => 'textarea',
    '#description' => t('Choose pages to display the World Flags logo or pages not to display.'),
    '#default_value' => isset($form_state['step_information']['1']['stored_values']['world_flags_logo_visibility_override_pages_list']) ?
      $form_state['step_information']['1']['stored_values']['world_flags_logo_visibility_override_pages_list'] :
      variable_get('world_flags_logo_visibility_override_pages_list', ''),
  );

  return $form;
}

function form_world_flags_wizard_submit($form, &$form_state) {
  $current_step = &$form_state['step'];
  $form_state['step_information'][$current_step]['stored_values'] = $form_state['values'];

  // Take all the variables generated in previous forms and
  // determine if it is important to save the variables.
  $last_form = count($form_state['step_information']);
  for ($i=1; $i<=$last_form; $i++) {
    $values = $form_state['step_information'][$i]['stored_values'];
    foreach ($values as $key => $value) {
      // Other values are coming in from previous form so ignore non-variables.
      if (! ($key == 'form_id' ||
        $key == 'form_build_id' ||
        $key == 'form_token' ||
        $key == 'next' ||
        $key == 'prev' ||
        $key == 'finish' ||
        $key == 'op') ) {
          variable_set($key, $value);
      }
    }
  }
  drupal_set_message(t('Your settings have been saved.'), 'status');
  if (! isset($_SESSION['iptolocation']['CountryCode2']) && (! isset($_SESSION['iptolocation']['RegionName']))) {
    drupal_set_message(t('The api supplied by easyjquery.com is currently down.') . '<br>' . t('No action from you is required. It should be up soon.'));
  }
}
