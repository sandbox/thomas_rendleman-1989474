Description:
------------
Uses IP to Location module to see where your user is located.
Then dynamically displays an appropriate flag for the user's location.
The flag can override the logo, override the favicon, or be displayed in
a block.

Requirements:
-------------
Drupal 7.x
IP to Location Module found at http://drupal.org/project/iptolocation

Quick installation:
0. Copy the extracted world_flags module into your server's
   directory at location sites/all/modules directory.

1. Copy the extracted IP to Location module found at
   http://drupal.org/project/IPtoLocation into your server's directory
   at location sites/all/modules directory.

2. Login as an administrator. Enable the above modules at
  http://www.example.com/?q=admin/modules

3. Once this is complete go to your home page or front page of your
   domain. If you see a flag flying in the logo area then everything is
   working. The default flag just shows until you change the settings in step 5.
   Sometimes if you are developing a website that uses a private ip
   then the IP detection may not happen. To correct this you need to install
   on a complete networked server so the IP detection will work. Development
   machines sometimes don't pickup the ip detection like an active live
   website will.

4. If you are going to use the World Flags in your logo area then Go to
   http://www.example.com/?q=admin/appearance/settings. Otherwise skip
   this step.

  a. Top right you can choose Global settings to show flags to all themes.
     You alternatively select a single theme.

  b. Unselect to use the default logo checkbox and a download text box will
     show up right below. It will be titled Upload Logo Image.

  c. Upload Logo Image. Click into that text box and select your logo to upload.
     In situations where the World Flags don't display then this logo will
     appear. It is always good to have a backup logo.

5. Go to http://www.example.com/?q=admin/config/regional/world_flag.
   Choose your options for displaying the World Flag Logo.

  a. You can choose to display World Flags in a block, favicon, and/or logo.
     You can choose a badge, non-animated graphics, or animated graphic
     for those displays.

6. If you are going to use the World Flags in a block then Go to
   http://www.example.com/?q=admin/structure/block. Otherwise you are done.


About this module and graphics:
-------------------------------
For those that have a history in graphics:
The only transparent graphic is the badge graphic. For the other graphics,
I would have left the background as transparent but the alias (jagged edges)
was too difficult to get rid of because some people's websites have different
patterns or color schemes. After rendering at lower resolutions and extremely
high movie studio quality resolutions I just wasn't satisfied with the alias
so I had to place a background. If you need a different background an
alternative would be to change the website css so that it matches better
or ask me for a modification.


Support:
--------
Please use the issue queue for filing bugs with this module at
http://drupal.org/project/issues/world-flags


Co-Authors of World Flags:
--------------------------
Thomas Rendleman (thomas_rendleman)
D34dMan * Special thank you for helping this module be better.
emosbaugh * Special thank you for helping me to use your modules 
  functionality.


Creator of the graphics:
------------------------
Thomas Rendleman <admin@businesswebsystem.com>
The graphics were created using Blender. Blender is an open source 3D
graphic program that can be used on Linux, Mac, and Windows. Enjoy.


Your Input:
-----------
We welcome ANY requests, feature ideas, or comments. :) We are also
available for creation/modification/customization of code or studio
quality graphics at reasonable pricing.
